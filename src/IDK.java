import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import javafx.scene.input.KeyCode;
import javax.swing.SwingConstants;


public class IDK {

	public static JFrame frame;
	private JTextField textField_1;

	private static String k;
	private JPasswordField passwordField;
	private static String p;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IDK window = new IDK();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the application.
	 */
	public IDK() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 323, 174);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 307, 135);
		frame.getContentPane().add(panel);
		
		textField_1 = new JTextField();
		textField_1.setBounds(82, 23, 86, 20);
		textField_1.setColumns(10);
		
		JButton btnbutton1 = new JButton("Login");
		btnbutton1.setBounds(188, 61, 109, 23);
		btnbutton1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnbutton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(login(textField_1.getText(), String.valueOf(passwordField.getPassword()))) {
					//setKennung(textField_1.getText());
					frame.setVisible(false);
					Profil.main(null);
				}
				else {
					System.out.println("fail, try again");
				}
				
				
				
			}
		});
		btnbutton1.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if(login(textField_1.getText(), String.valueOf(passwordField.getPassword()))) {
						frame.setVisible(false);
						Profil.main(null);
					}
				}
				
			}
		});
		
		JLabel lblNewLabel = new JLabel("Passwort");
		lblNewLabel.setBounds(15, 65, 61, 14);
		
		JLabel lblNewLabel_1 = new JLabel("Kennung");
		lblNewLabel_1.setBounds(15, 26, 61, 14);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(82, 62, 86, 20);
		
		JButton btnRegistrieren = new JButton("Registrieren");
		btnRegistrieren.setBounds(188, 106, 109, 23);
		btnRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				register.main(null);
			}
		});
		
		JLabel lblSieAbenNoch = new JLabel("Noch kein Account?");
		lblSieAbenNoch.setBounds(59, 110, 153, 14);
		panel.setLayout(null);
		panel.add(lblNewLabel);
		panel.add(lblNewLabel_1);
		panel.add(passwordField);
		panel.add(textField_1);
		panel.add(btnbutton1);
		panel.add(lblSieAbenNoch);
		panel.add(btnRegistrieren);
		panel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{textField_1, passwordField, btnbutton1}));
		
		
		}
		
	public static boolean login(String Kennung, String Passwort) {
		SQL s = new SQL();
		s.getConnection("jdbc:mysql://84.38.65.33/Q11USR", "Q11USR", "q11_1718"); 
	    if(s.einzelStatement("SELECT * FROM SHAMML_Passwort WHERE Kennung = '" + Kennung + "'" , "Passwort").equals(Passwort)) {
	    	k = Kennung;
	    	p = Passwort;
	    	s.closeConnection();
	    	return true;
	    }else {
	    	s.closeConnection();
	    	return false;
	    }
	}
	public String getKennung() {
		return k;
	}
	
	public void setKennung(String Kennung) {
		k = Kennung;
	}
	
	public String getPassword() {
		return p;
	}
}

	


