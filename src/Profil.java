import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableModel;

import org.eclipse.wb.swing.FocusTraversalOnArray;

public class Profil extends JFrame{
	private JFrame frame;
	private String vorname;
	private String nachname;
	private String strecke;
	private String schule;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Profil window = new Profil();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Profil() {
		ziehen();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("serial")
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 704, 387);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLayeredPane panel = new JLayeredPane();
		frame.getContentPane().add(panel, BorderLayout.CENTER);

		JLabel lblNewLabel = new JLabel("Vorname");
		lblNewLabel.setBounds(10, 66, 75, 14);

		JLabel lblNewLabel_1 = new JLabel("Nachname");
		lblNewLabel_1.setBounds(10, 86, 75, 14);

		JLabel lblNewLabel_2 = new JLabel("Zur�ckgelegte Strecke");
		lblNewLabel_2.setBounds(10, 106, 156, 14);

		JLabel lblNewLabel_3 = new JLabel("Schule");
		lblNewLabel_3.setBounds(10, 126, 94, 14);

		JLabel lblNewLabel_4 = new JLabel(vorname);
		lblNewLabel_4.setBounds(145, 66, 100, 14);
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblNewLabel_5 = new JLabel(nachname);
		lblNewLabel_5.setBounds(145, 86, 100, 14);
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblNewLabel_6 = new JLabel(strecke);
		lblNewLabel_6.setBounds(145, 106, 100, 14);
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblNewLabel_7 = new JLabel(schule);
		lblNewLabel_7.setBounds(145, 126, 100, 14);
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblNewLabel_8 = new JLabel("Profil");
		lblNewLabel_8.setBounds(10, 26, 219, 22);
		lblNewLabel_8.setFont(new Font("Tahoma", Font.PLAIN, 18));

		JButton btnChangePassword = new JButton("Change Password");
		btnChangePassword.setBounds(20, 204, 146, 23);
		btnChangePassword.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JButton btnChangeUsername = new JButton("Change Username");
		btnChangeUsername.setBounds(20, 170, 146, 23);
		btnChangeUsername.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JButton btnAddDistance = new JButton("Add Distance");
		btnAddDistance.setBounds(20, 238, 146, 23);
		btnAddDistance.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		SQL s = new SQL();
		s.getConnection("jdbc:mysql://84.38.65.33/Q11USR", "Q11USR", "q11_1718");
		String[][] rankArray = s
				.query("SELECT `Vorname`, `Nachname`, `GefahreneStrecke`, `Schule` FROM `SHAMML_Passwort`", 4);

		table = new JTable();
		table.setBorder(new CompoundBorder());
		table.setToolTipText("Ranking");
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(

				new String[] { "Vorname", "Nachname", "Gefahrene Strecke", "Schule" }, 0) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] { String.class, String.class, Integer.class, String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});

		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		table.getColumnModel().getColumn(2).setPreferredWidth(120);
		table.getColumnModel().getColumn(3).setPreferredWidth(250);

		DefaultTableModel model = (DefaultTableModel) table.getModel();
		for (int i = 0; i < rankArray.length; i++) {
			model.addRow(rankArray[i]);
		}
		Vector<Vector<String>> data = model.getDataVector();
		data.sort((v1, v2) -> (Integer.parseInt(v2.get(2)) - Integer.parseInt(v1.get(2))));

		table.setToolTipText("Ranking");
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setBorder(new CompoundBorder());
		table.setBounds(356, 66, 378, 160);
		// panel.add(new JScrollPane(table));

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(293, 65, 385, 174);
		scrollPane.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel.setLayout(null);
		panel.add(btnChangeUsername);
		panel.add(btnChangePassword);
		panel.add(btnAddDistance);
		panel.add(lblNewLabel);
		panel.add(lblNewLabel_8);
		panel.add(lblNewLabel_2);
		panel.add(lblNewLabel_3);
		panel.add(lblNewLabel_1);
		panel.add(lblNewLabel_7);
		panel.add(lblNewLabel_6);
		panel.add(lblNewLabel_4);
		panel.add(lblNewLabel_5);
		panel.add(scrollPane);

		JComboBox comboBox = new JComboBox();
		comboBox.setToolTipText("Sortierung \u00E4ndern");
		comboBox.setModel(
				new DefaultComboBoxModel(new String[] { "Gefahrene Distanz", "Vorname", "Nachname", "Schule" }));
		comboBox.setBounds(329, 30, 156, 20);
		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				String item = (String) comboBox.getSelectedItem();
				Vector<Vector<String>> vec = model.getDataVector();

				switch (item) {
				case "Gefahrene Distanz":
					vec.sort((v1, v2) -> (Integer.parseInt(v2.get(2)) - Integer.parseInt(v1.get(2))));
					break;

				case "Nachname":
					vec.sort((v1, v2) -> (v1.get(1).compareTo(v2.get(1))));
					break;

				case "Vorname":
					vec.sort((v1, v2) -> (v1.get(0).compareTo(v2.get(0))));
					break;

				case "Schule":
					vec.sort((v1, v2) -> (v1.get(3).compareTo(v2.get(3))));
					break;

				default:
				}

				table.updateUI();
			}
		});
		panel.add(comboBox);
		panel.setFocusTraversalPolicy(
				new FocusTraversalOnArray(new Component[] { btnChangeUsername, btnChangePassword, btnAddDistance }));

		btnChangePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// frame.setVisible(false);
				ChangePasswordFrame cpf = new ChangePasswordFrame();
				cpf.setVisible(true);
			}

		});

		btnChangeUsername.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// frame.setVisible(false);
				ChangeUsernameFrame cuf = new ChangeUsernameFrame();
				cuf.setVisible(true);
				frame.dispose();
			}
		});

		btnAddDistance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				frame.setVisible(false);
				AddDistanceFrame adf = new AddDistanceFrame();
				adf.setVisible(true);
			}
		});

		// public void populateJList() {

		// }

	}

	public void ziehen() {
		SQL s = new SQL();
		IDK i = new IDK();
		s.getConnection("jdbc:mysql://84.38.65.33/Q11USR", "Q11USR", "q11_1718");
		vorname = s.einzelStatement("SELECT * FROM SHAMML_Passwort WHERE Kennung = '" + i.getKennung() + "'",
				"Vorname");
		nachname = s.einzelStatement("SELECT * FROM SHAMML_Passwort WHERE Kennung = '" + i.getKennung() + "'",
				"Nachname");
		strecke = s.einzelStatement("SELECT * FROM SHAMML_Passwort WHERE Kennung = '" + i.getKennung() + "'",
				"GefahreneStrecke");
		schule = s.einzelStatement("SELECT * FROM SHAMML_Passwort WHERE Kennung = '" + i.getKennung() + "'", "Schule");
		s.closeConnection();
	}

	public void refresh() {
		this.main(null);
	}


}