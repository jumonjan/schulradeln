import java.sql.*;
import java.util.ArrayList;


public class SQL {
  private String driver = "com.mysql.jdbc.Driver";
  private Connection conn;

  public void getConnection(String path, String user, String password) {
    try {
      Class.forName(driver);
      System.out.println("driver loaded");
    } catch (Exception e) {
      System.out.println("fail: " + e);
      System.exit(-1);
    }

    try {
      conn = DriverManager.getConnection(path, user, password);
    } catch (Exception e) {
      System.out.println("fail: " + e);
    }
  }

  public void closeConnection() {
    try {
      conn.close();
    } catch (Exception e) {
      System.out.println("fail: " + e);
    }
  }

  public void statementAbsetzen(String statement, String feld) {
    ResultSet sqlResult;
    Statement stmt;
    try {
      stmt = conn.createStatement();
      sqlResult = stmt.executeQuery(statement);
      System.out.println("statement " + statement + " done, result: " + feld);
      while (sqlResult.next()) {
        System.out.println(sqlResult.getString(feld));
      }
      sqlResult.close();
      stmt.close();
    } catch (Exception e) {
      System.out.println("fail: " + e);
    }
  }

  public void instructionAbsetzen(String instruction) {
    Statement stmt;
    try {
      stmt = conn.createStatement();
      stmt.executeUpdate(instruction);
      stmt.close();
      System.out.println("instruction " + instruction + " erfolgreich abgesetzt");
    } catch (Exception e) {
      System.out.println("Fehler bei SQL instruction\n" + e);
    }
  }

  public String einzelStatement(String statement, String feld) {
    ResultSet sqlResult;
    Statement stmt;
    String s = new String();
    try {
      stmt = conn.createStatement();
      sqlResult = stmt.executeQuery(statement);
      System.out.println("statement " + statement + " done, result: " + feld);
      while (sqlResult.next()) {
        s = sqlResult.getString(feld);
      }
      sqlResult.close();
      stmt.close();
    } catch (Exception e) {
      System.out.println("fail: " + e);
    }
    return s;
  }

  public String[][] query(String statement, int columnCount) {
    ArrayList<String[]> list = new ArrayList<>();
    ResultSet sqlResult;
    Statement stmt;
    try {
      stmt = conn.createStatement();
      sqlResult = stmt.executeQuery(statement);
      // System.out.println("statement " + statement + " done, result: " + feld);
      while (sqlResult.next()) {
        String[] row = new String[columnCount];
        for (int i = 0; i < columnCount; i++) {
          row[i] = sqlResult.getString(i+1);
        }
        list.add(row);
      }
      sqlResult.close();
      stmt.close();

      String[][] result = new String[list.size()][columnCount];
      result = list.toArray(result);
      return result;
    } catch (Exception e) {
      e.printStackTrace(System.err);
    }
    return null;
  }

  public static void main(String[] args) {
    SQL s = new SQL();
    s.getConnection("jdbc:mysql://84.38.65.33/Q11USR", "Q11USR", "q11_1718");
    s.statementAbsetzen("SELECT * FROM SHAMML_Passwort WHERE Kennung = 'AD234';", "Passwort");
    s.closeConnection();
  }
}
